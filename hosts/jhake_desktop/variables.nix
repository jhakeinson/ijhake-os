{
  # Git Configuration ( For Pulling Software Repos )
  gitUsername = "Jhake Inson";
  gitEmail = "mr@jhakeinson";

  # Base16 Theme
  theme = "dracula";

  # Hyprland Settings
  borderAnim = false; # Enable / Disable Hyprland Border Animation
  extraMonitorSettings = "";

  # Waybar Settings
  clock24h = false;
  waybarAnimations = false;

  # Program Options
  browser = "firefox"; # Set Default Browser (google-chrome-stable for google-chrome)
  terminal = "kitty"; # Set Default System Terminal
}
